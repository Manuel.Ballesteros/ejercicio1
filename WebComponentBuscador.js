class Buscador extends HTMLElement {
    constructor() {
    super();
    const tpl = document.querySelector('#Buscador');
    const tplInst = document.importNode(tpl.content, true);

    this.attachShadow({mode: 'open'});
    this.shadowRoot.appendChild(tplInst);
    this._value = '';
    }
    get _value() {
        return this.getAttribute('value');
    }
    set _value(value) {
        return this.setAttribute('value', value);
    }
}
customElements.define('buscador-', Buscador);
