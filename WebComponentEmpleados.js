class tablaEmpleados extends HTMLElement {
    constructor() {
        super();
        
        this.attachShadow({mode: 'open'});
        
        this.tpl = document.querySelector('#tablaEmpleados');
        this.tplInst = document.importNode(this.tpl.content, true);
    }
    connectedCallback() {
        this.shadowRoot.appendChild(this.tplInst);

        fetch('http://dummy.restapiexample.com/api/v1/employees')
        .then((response) => {
            if (response.ok) {
                return response.json();
            } else {
                throw new Error('Error fetch');
            }
        })
        .then((data) => {
            this.employees = data.data;
            this.listEmployees(this.employees);
        })
        .catch(err => {
            console.error(err.message);
        });

        document.addEventListener('search', this);
    }

    listEmployees(data) {
        this.$table = this.shadowRoot.querySelector('table');
        this.$tbody = this.shadowRoot.querySelector('table tbody');
        const $fragment = document.createDocumentFragment();

     data.forEach(employee => {
    const $tr = document.createElement('tr');
            for (const celda in employee) {
                const $td = document.createElement('td');
                $td.textContent = employee[celda];
                $tr.appendChild($td);
            }
            $fragment.appendChild($tr);
        });
        this.$tbody.replaceChildren($fragment);
    }

    BuscaEmpleado(value) {
        const encuentraEmpleado = this.employees.filter(employee => (new RegExp(value)).test(employee.employee_name));
        this.listEmployees(encuentraEmpleado);
    }
}

customElements.define('tabla-empleados', tablaEmpleados);